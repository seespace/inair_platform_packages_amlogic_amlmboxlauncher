package com.amlogic.launcher;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.ComponentName;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.SystemProperties;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;


import android.view.View;
import android.view.KeyEvent;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.RelativeLayout;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.AnimationDrawable;

import android.media.AudioManager;
import android.media.SoundPool;

import android.provider.Settings;

import android.util.Log;


import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.Runnable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Calendar;
import java.lang.Exception;


public class Launcher extends Activity {
    /** Called when the activity is first created. */
	private static final String TAG = "Launcher";
	
	private ImageButton btn_home = null;
	private TextView tx_home = null;
	private GridView gv = null;
	private GridView lv_status = null;
	private GridView lv_menu = null;

	private  AnimationDrawable anim_selector_1 = null;
	private  AnimationDrawable anim_selector_2 = null;
	
	private String[] list_custom_apps;
	
	private boolean into_mate_activity = false;
	
	private  AnimationDrawable anim_weather = null;

	private GetWeather mWeather = null;
	private String str_province;
	private String str_city;
	private String str_area;
	private String str_come_from;
	private String str_tem;
	private String str_dunhao;
	private String weather_status = null;
	private String weather_status_detail = null;
	private RelativeLayout layout_main = null;
	private int time_count = 0;

	private static SoundPool sp_foucs_change =null;
	private static SoundPool sp_button = null;
	private static int music_prio_focus;
	private static int music_prio_button;
	private static boolean isSystemSoundOn;
	
	private final static String CUSTOM_APPS_PATH = "/data/data/com.amlogic.launcher/custom_apps.cfg";
	private final static String WEATHER_PATH_0 = "/data/data/com.amlogic.launcher/weather_0.cfg";
	private final static String WEATHER_PATH_1 = "/data/data/com.amlogic.launcher/weather_1.cfg";
	private final static String WEATHER_PATH_2 = "/data/data/com.amlogic.launcher/weather_2.cfg";
	private final static String FACTORY_WALLPAPER_PATH = "/data/data/com.amlogic.launcher/factory_wallpaper.jpg";
	private final String DATA_PATH = "/data/data/com.amlogic.launcher/";
	private final String HIDE_STATUSBAR = "sys.hideStatusBar.enable";
	private final String ENABLE_ANIMATION = "dongleLauncher.enable.Animation";
	
	public final static String WALLPAPER_PATH = "/data/data/com.amlogic.launcher/wallpaper.cfg";
	public final static int UPDATE_INFO = 0x54123;
	public final static int UPDATE_WEATHER = 0x54124;
	public static String net_change_action = "android.net.conn.CONNECTIVITY_CHANGE";
	public static String wifi_signal_action = "android.net.wifi.RSSI_CHANGED";
	public static String city = null;
	public static String temperature = null;
	public static Drawable img_weather = null;
	public static Drawable img_bg = null;
	public static Bitmap bmp_bg = null;
	public static boolean is24hFormart = false; 
	public static boolean hasAnimation = true; 

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		Log.d(TAG, "------onCreate");
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	 	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);  
        setContentView(R.layout.main);
		
		layout_main = (RelativeLayout)findViewById(R.id.layout_main);
		if (loadWallpaper() != -1){
			layout_main.setBackgroundDrawable(img_bg);
		}else if (loadFactoryWallpaper() != -1){
			layout_main.setBackgroundDrawable(img_bg);
		}
		else {
			img_bg = null;
		}
		
		SystemProperties.set(HIDE_STATUSBAR,"true");
		hasAnimation = SystemProperties.getBoolean(ENABLE_ANIMATION, true);

		isSystemSoundOn = isSystemSoundEnabled();
		sp_foucs_change = new SoundPool(10, AudioManager.STREAM_SYSTEM, 5);
        music_prio_focus = sp_foucs_change.load(this, R.raw.sound_focus_change, 1); 
		sp_button = new SoundPool(10, AudioManager.STREAM_SYSTEM, 5);
        music_prio_button = sp_button.load(this, R.raw.sound_button, 1); 
		
		tx_home = (TextView)findViewById(R.id.tx_home);
		tx_home.setBackgroundResource(R.drawable.bg_menu_tx);

		lv_status = (GridView)findViewById(R.id.list_status);		

		gv = (GridView)findViewById(R.id.grid_custom_apps);
		anim_selector_1 = (AnimationDrawable)gv.getSelector();
		
		gv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(pos);

				playClickMusic();
				
                if(item.get("item_type").equals(R.drawable.item_img_add)) {
					into_mate_activity = true;
		 			Intent intent = new Intent();
					intent.setClass(Launcher.this, CustomAppsActivity.class);
			  		startActivity(intent);
		      	  
             	} else {
			  		
	            	startActivity((Intent)item.get("file_path"));
			  	}
          
            }
        });
		
		lv_menu = (GridView)findViewById(R.id.list_menu);
		anim_selector_2 = (AnimationDrawable)lv_menu.getSelector();
		
		lv_menu.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(pos);
				Intent intent = new Intent();

				playClickMusic();
				
				//into_mate_activity = true;	
		            if(item.get("item_type").equals(R.drawable.btn_apps)) {
					  	intent.setClass(Launcher.this, AllApps.class);
					  	startActivity(intent);
		            }else if(item.get("item_type").equals(R.drawable.btn_local)){
						intent.setClass(Launcher.this, LocalActivity.class);
						startActivity(intent);
					}else if(item.get("item_type").equals(R.drawable.btn_browser)){
						intent.setClass(Launcher.this, BrowserActivity.class);
						startActivity(intent);
					}else if(item.get("item_type").equals(R.drawable.btn_setting)){
						intent .setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
						startActivity(intent);
					}   
			     
            }
        });

		gv.setOnKeyListener(myOnKeylistener);
		lv_menu.setOnKeyListener(myOnKeylistener);

		animStart();
		displayWeather();
		displayStatus();
		displayView();
		displayDate();
		displayMenu();
		
    }

	

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "------onResume");
		SystemProperties.set(HIDE_STATUSBAR,"true");
		into_mate_activity = false;

         if(SystemProperties.getBoolean("ro.platform.has.mbxuimode", false)){
            if(SystemProperties.getBoolean("ubootenv.var.has.accelerometer", true)
                            && SystemProperties.getBoolean("sys.keeplauncher.landcape", false))
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            else
               setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
         }

		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_MEDIA_EJECT);
		filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
		filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
		filter.addDataScheme("file");
		registerReceiver(mediaReceiver, filter);

		filter = new IntentFilter();
		filter.addAction(net_change_action);
		filter.addAction(wifi_signal_action);
		filter.addAction(WifiManager.WIFI_AP_STATE_CHANGED_ACTION);
		filter.addAction(Intent.ACTION_TIME_TICK);	
		registerReceiver(netReceiver, filter);
		
		isSystemSoundOn = isSystemSoundEnabled();
		displayView();
		displayDate();
		
		animStart();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "------onPause");
		if(!into_mate_activity) {
			SystemProperties.set(HIDE_STATUSBAR,"false");
			into_mate_activity = false;
		}
 
		unregisterReceiver(mediaReceiver);
		unregisterReceiver(netReceiver);
		animStop();
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "------onStop");
	
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "------onDestroy");
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

	  if(keyCode == KeyEvent.KEYCODE_BACK) {
		 return true;
	  }
      
      if(keyCode == KeyEvent.KEYCODE_SEARCH) {        
          SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
          ComponentName globalSearchActivity = searchManager.getGlobalSearchActivity();
          if (globalSearchActivity == null) {
              Log.w(TAG, "No global search activity found.");
             return false;
         }
          
         Intent intent = new Intent(SearchManager.INTENT_ACTION_GLOBAL_SEARCH);
         intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         intent.setComponent(globalSearchActivity);
         Bundle appSearchData = new Bundle();
         appSearchData.putString("source", "launcher-search");
         intent.putExtra(SearchManager.APP_DATA, appSearchData);
         startActivity(intent);
		 return true;
	  }
      
		 return super.onKeyDown(keyCode, event);
	}
	
	private  void animStart(){
		if (hasAnimation == true){
			anim_selector_1.start();
			anim_selector_2.start();
		} else {
			anim_selector_1.stop();
			anim_selector_2.stop();
		}
	}
	private  void animStop(){
		anim_selector_1.stop();
		anim_selector_2.stop();
		anim_selector_1.setCallback(null);
		anim_selector_2.setCallback(null);
	}

	private void displayStatus() {

		WifiManager mWifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
        WifiInfo mWifiInfo = mWifiManager.getConnectionInfo();
        int wifi_rssi = mWifiInfo.getRssi();
		int wifi_level = WifiManager.calculateSignalLevel(
                        wifi_rssi, 5);
	 
		 LocalAdapter ad = new LocalAdapter(Launcher.this,
						 getStatusData(wifi_level, isEthernetOn()),
						 R.layout.homelist_item, 			 
									 new String[] {"item_type", "item_name", "item_sel"},
									 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
		 lv_status.setAdapter(ad);
		 lv_status.setFocusable(false);
	 }

	 
	 private void updateStatus() {
		   ((BaseAdapter) lv_status.getAdapter()).notifyDataSetChanged();
	  }

	 public static List<Map<String, Object>> getStatusData(int wifi_level, boolean is_ethernet_on) {
	 
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();  
		Map<String, Object> map = new HashMap<String, Object>();

	

		//Log.d(TAG, "wifi strength = " + wifi_level);
		
		switch (wifi_level) {
			case 0:
			 	map.put("item_type", R.drawable.wifi1);				
				break;
			case 1:
				map.put("item_type", R.drawable.wifi2);;
				break;
			case 2:
			 	map.put("item_type", R.drawable.wifi3);
				break;
			case 3:
			 	map.put("item_type", R.drawable.wifi4);
				break;
			case 4:
			 	map.put("item_type", R.drawable.wifi5);
				break;
			default:
				break;
		}
		list.add(map);

		// Log.d(TAG, " @@@@@@@@@@@@@ 1 = " + isSdcardExists() + "  2 = " + sdcardSize()  + "  3 = "	+ innerSdcardSize() + "    4 =" + isUsbExists());
		 
		 if(isSdcardExists() == true){
		 	map = new HashMap<String, Object>();
			map.put("item_type", R.drawable.img_status_sdcard);
			list.add(map);
		}

		 if(isUsbExists() == true){
		 	 map = new HashMap<String, Object>();
			 map.put("item_type", R.drawable.img_status_usb);
			 list.add(map);
		 }
/*
		 if(isSataExists() == true){
		 	 map = new HashMap<String, Object>();
			 map.put("item_type", R.drawable.img_status_sata);
			 list.add(map);
		 }
*/
		if(is_ethernet_on == true){
			map = new HashMap<String, Object>();
			map.put("item_type", R.drawable.img_status_ethernet);
			list.add(map);
		}
		
		return list;
	}	 
	 
	private void displayDate() {
		int[] array = getTime();

		is24hFormart = DateFormat.is24HourFormat(this); 
		
		ImageView hour_1 = (ImageView)findViewById(R.id.img_hour_1);	
		ImageView hour_2 = (ImageView)findViewById(R.id.img_hour_2);
		ImageView colon = (ImageView)findViewById(R.id.img_colon);
		ImageView minu_1 = (ImageView)findViewById(R.id.img_minu_1);
		ImageView minu_2 = (ImageView)findViewById(R.id.img_minu_2);	
		ImageView day = (ImageView)findViewById(R.id.img_day);
		TextView  date = (TextView)findViewById(R.id.tx_date);

		setImage(hour_1, array[0]);
		setImage(hour_2, array[1]);
		colon.setImageResource(R.drawable.colon);
		setImage(minu_1, array[2]);
		setImage(minu_2, array[3]);

		if (array[4] != -1) {
			day.setVisibility(View.VISIBLE);
			if (array[4] == 0) {
				day.setImageResource(R.drawable.am);
			} else{
				day.setImageResource(R.drawable.pm);
			}
		} else {
			day.setVisibility(View.GONE);
		}

		date.setText(getDate());
		date.setTypeface(Typeface.DEFAULT_BOLD);
		date.setTextSize(15);

	}

	private void displayWeather() {

		mHandler.sendEmptyMessage(UPDATE_WEATHER);
		
		if (isConnectingToInternet() == true) {
			str_province = getString(R.string.str_province);
			str_city = getString(R.string.str_city);
			str_area = getString(R.string.str_area);
			str_come_from = getString(R.string.str_come_from);
			str_tem = getString(R.string.str_tem);
			str_dunhao = getString(R.string.str_dunhao);
			mWeather = new GetWeather(this);
			mThread  th = new mThread();
			th.start();
		} else {
			TextView today_main = (TextView)findViewById(R.id.tx_today_main);
			today_main.setText(getString(R.string.str_fail_load_weather));
			today_main.setTextSize(14);
			today_main.setTextColor(Color.RED);

		}
	}
	
	private void setImage(Object data, int i){
		ImageView img = (ImageView)data;

		switch(i){
			case 0:
				img.setImageResource(R.drawable.img_0);
				break;
			case 1:
				img.setImageResource(R.drawable.img_1);
				break;
			case 2:
				img.setImageResource(R.drawable.img_2);
				break;
			case 3:
				img.setImageResource(R.drawable.img_3);
				break;
			case 4:
				img.setImageResource(R.drawable.img_4);
				break;
			case 5:
				img.setImageResource(R.drawable.img_5);
				break;
			case 6:
				img.setImageResource(R.drawable.img_6);
				break;
			case 7:
				img.setImageResource(R.drawable.img_7);
				break;
			case 8:
				img.setImageResource(R.drawable.img_8);
				break;
			case 9:
				img.setImageResource(R.drawable.img_9);
				break;	
			default:
				break;
		}

	} 
		
	private void displayMenu() {
		 LocalAdapter ad = new LocalAdapter(Launcher.this,
						 getMenuData(),
						 R.layout.list_menu, 			 
									 new String[] {"item_type", "item_name", "item_sel"},
									 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
		 lv_menu.setAdapter(ad);
		 lv_menu.setSelection(2);
		 lv_menu.requestFocus();
	 }

	public static List<Map<String, Object>> getMenuData() {
	 
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();  
		Map<String, Object> map = new HashMap<String, Object>();
	
			map.put("item_type", R.drawable.btn_local);
			list.add(map);

			map = new HashMap<String, Object>();
			map.put("item_type", R.drawable.btn_apps);
			list.add(map);
			
			map = new HashMap<String, Object>();
			map.put("item_type", R.drawable.btn_home);
			list.add(map);
			
			map = new HashMap<String, Object>();
			map.put("item_type", R.drawable.btn_browser);
			list.add(map);
			
			map = new HashMap<String, Object>();
			map.put("item_type", R.drawable.btn_setting);
			list.add(map);
			
		return list;
	}	

	private void displayView() {
	 
		 LocalAdapter ad = new LocalAdapter(Launcher.this,
						 loadApplications(),
						 R.layout.homegrid_item, 			 
									 new String[] {"item_type", "item_name", "item_sel"},
									 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
		 gv.setAdapter(ad);
	 }
	 
	
	private List<Map<String, Object>> loadApplications() {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();	
		Map<String, Object> map = new HashMap<String, Object>();

        PackageManager manager = getPackageManager();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        final List<ResolveInfo> apps = manager.queryIntentActivities(mainIntent, 0);
        Collections.sort(apps, new ResolveInfo.DisplayNameComparator(manager));

		list_custom_apps = loadCustomApps();
        if (apps != null) {
			
            final int count = apps.size();
			
            for (int i = 0; i < count; i++) {
                ApplicationInfo application = new ApplicationInfo();
                ResolveInfo info = apps.get(i);
		
                application.title = info.loadLabel(manager);
                application.setActivity(new ComponentName(
                        info.activityInfo.applicationInfo.packageName,
                        info.activityInfo.name),
                        Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                application.icon = info.activityInfo.loadIcon(manager);

				if(list_custom_apps != null){
					for (int j=0; j < list_custom_apps.length; j++){
						if (application.componentName.getPackageName().equals(list_custom_apps[j])){
							 map = new HashMap<String, Object>(); 
							 map.put("item_name", application.title.toString());   
							 map.put("file_path", application.intent); 		   
							 map.put("item_sel", R.drawable.item_img_unsel);
							 map.put("item_type", application.icon);
							 map.put("item_symbol", application.componentName);
							 list.add(map);
						}
					}
				}
                 

            }
        }

		map = new HashMap<String, Object>(); 
		map.put("item_name", getString(R.string.str_add));   
	    map.put("file_path", null); 		   
	 	map.put("item_sel", R.drawable.item_img_unsel);
		map.put("item_type", R.drawable.item_img_add);
		list.add(map);

		return list;
    }

	private String[] loadCustomApps(){
		 String[] list_custom_apps;
		
		 File mFile = new File(CUSTOM_APPS_PATH);
		
		if(!mFile.exists()) {
			return null;
		}
		
		try {
			FileReader fr = new FileReader(mFile);
			char[] buf = new char[5120];
			
			int len = fr.read(buf);

			String r_string = new String(buf, 0, len);
			//Log.d(TAG, "load string is " + r_string);

			list_custom_apps = r_string.split(";");
			if (list_custom_apps != null){
				for (int j=0; j < list_custom_apps.length; j++){
					Log.d(TAG, "              "+ list_custom_apps[j]);
				}

			}
		
			fr.close();
		}
		catch (Exception e) {
			return null;
		}
		return list_custom_apps;

	}

	public static boolean isUsbExists(){
		File dir = new File(LocalActivity.USB_PATH);  
		if (dir.exists() && dir.isDirectory()) {
			if (dir.listFiles() != null) {
				if (dir.listFiles().length > 0) {
					for (File file : dir.listFiles()) {
						String path = file.getAbsolutePath();
						if (path.startsWith(LocalActivity.USB_PATH+"/sd")&&!path.equals(LocalActivity.SD_PATH)) {
					//	if (path.startsWith("/mnt/sd[a-z]")){
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	public static boolean isSataExists(){
		File dir = new File(LocalActivity.SATA_PATH);  
		if (dir.exists() && dir.isDirectory()) {
			return true;
		}
		return false;
	}
	
	public static boolean isSdcardExists(){
		if(Environment.getExternalStorage2State().startsWith(Environment.MEDIA_MOUNTED)) {
			File dir = new File(LocalActivity.SD_PATH);  
			if (dir.exists() && dir.isDirectory()) {
				return true;
			}
		}
		return false;
		/*
		long sdsize = getDeviceSize(LocalActivity.SD_PATH);
		long nandsize = getDeviceSize(LocalActivity.NAND_PATH);
		Log.d(TAG, "@@@sdsize="+sdsize+",   nandsize="+nandsize);
		Log.d(TAG, "*****************external lable="+Environment.getExternalStorageLabel()+",  state2="+Environment.getExternalStorage2State()+",  state="+Environment.getExternalStorageState());
		Log.d(TAG, "*****************internal state="+Environment.getInternalStorageState());

		if((sdsize < 0) || (sdsize == nandsize))
			return false;
		else
			return true;
		*/
	}
	/*
	private static long getDeviceSize(String path){
	   File pathFile = new File(path);
	   if (pathFile.exists()){
	       android.os.StatFs statfs = new android.os.StatFs(pathFile.getPath());          
	       long nTotalBlocks = statfs.getBlockCount();          
	       long nBlocSize = statfs.getBlockSize();                   
	       long nAvailaBlock = statfs.getAvailableBlocks();        
	       long nFreeBlock = statfs.getFreeBlocks();              
	       long nTotalSize = nTotalBlocks * nBlocSize / 1024 / 1024; 
	       return nTotalSize; 
	   } else {
		   return -1;
	   }
	}
	*/

	private boolean isEthernetOn(){
		ConnectivityManager connectivity = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
	
		if (info.isConnected()){
			//Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ true");
			return true;
		} else {
		    	//Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ false");
			return false;
		}
	}

	 private boolean isConnectingToInternet(){
			ConnectivityManager connectivity = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
			  if (connectivity != null)
			  {
				  NetworkInfo[] info = connectivity.getAllNetworkInfo();
				  if (info != null)
				  	for (int i = 0; i < info.length; i++)
						  if (info[i].getState() == NetworkInfo.State.CONNECTED)
						  {
							  return true;
						  }
			  }
			  return false;
	}
	

	public static int[] getTime(){
		final Calendar c = Calendar.getInstance();
		int[] array = new int[5];
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);

		if (is24hFormart == false) {
			if (hour > 12) {
				hour = hour - 12;
				array[4] = 1;
			}else {
				array[4] = 0;
			}
		} else {
			array[4] = -1;
		}
		
		array[0] = hour / 10; 
		array[1] = hour % 10;
		array[2] = minute / 10;
		array[3] = minute % 10;

		
		
		//Log.d(TAG, "@@@@@@@@@@@@@@@@@@ " + array[0]+ array[1] + ":" + array[2] + array[3]);

		return array;
	}
	private String getDate(){
		final Calendar c = Calendar.getInstance(); 
		String mYear = Integer.toString(c.get(Calendar.YEAR));
        String mMonth = Integer.toString(c.get(Calendar.MONTH)+1); 
        String mDay = Integer.toString(c.get(Calendar.DAY_OF_MONTH)); 
		int int_Week = c.get(Calendar.DAY_OF_WEEK) -1; 
		String str_week =  this.getResources().getStringArray(R.array.week)[int_Week];

		
		String date = mYear + "." + mMonth + "." + mDay + "   " + str_week;

		//Log.d(TAG, "@@@@@@@@@@@@@@@@@@@ "+ date  + "week = " +int_Week);
		return date;
	}

	private Handler timeHandler=new Handler();
	
	private Runnable runnable=new Runnable(){
		@Override
		public void run() {
			// TODO Auto-generated method stub
		
			Message msg = new Message();
			msg.what = Launcher.UPDATE_INFO;
			mHandler.sendMessage(msg);

			time_count++;
			if(time_count >= 60){
				mHandler.sendEmptyMessage(UPDATE_WEATHER);
			}
			
			timeHandler.postDelayed(this, 60000);
		}
	};
	


	private void setTodayTem(){	
		String[] list_weather_info = loadWeather(WEATHER_PATH_0);
		if ((list_weather_info == null) || (list_weather_info.length < 5)){
			return;
		}
		
		TextView today_main = (TextView)findViewById(R.id.tx_today_main);
		today_main.setText(list_weather_info[1] + " " + list_weather_info[0]);
		today_main.setTypeface(Typeface.MONOSPACE,Typeface.BOLD_ITALIC);
		today_main.setTextSize(18);
		today_main.setTextColor(Color.WHITE);

		TextView today_2 = (TextView)findViewById(R.id.tx_0_2);
		TextView today_3 = (TextView)findViewById(R.id.tx_0_3);
		TextView today_4 = (TextView)findViewById(R.id.tx_0_4);
		today_2.setText(list_weather_info[1]);
		today_3.setText(list_weather_info[3]);
		today_4.setText(list_weather_info[4]);

		ImageView weather = (ImageView)findViewById(R.id.img_weather);
		ImageView today_1 = (ImageView)findViewById(R.id.img_0_1);
		String weather_status = list_weather_info[2];
		boolean isWeatherValid = true;
		
		if (weather_status.equals(getString(R.string.str_cloudy))){
			weather.setImageResource(R.anim.anim_weather_cloudy);
			today_1.setImageResource(R.drawable.cloudy03);
		} else if (weather_status.equals(getString(R.string.str_sunny))){
			weather.setImageResource(R.anim.anim_weather_sunny);
			today_1.setImageResource(R.drawable.sunny03);
		} else if (weather_status.equals(getString(R.string.str_smoke))){
			weather.setImageResource(R.anim.anim_weather_smoke);
			today_1.setImageResource(R.drawable.smoke03);
		} else if (weather_status.equals(getString(R.string.str_shade))){
			weather.setImageResource(R.anim.anim_weather_shade);
			today_1.setImageResource(R.drawable.shade03);
		} else if (weather_status.equals(getString(R.string.str_h_sand_storm))){
			weather.setImageResource(R.anim.anim_weather_h_sand_storm);
			today_1.setImageResource(R.drawable.h_sand_storm03);
		} else if (weather_status.equals(getString(R.string.str_sand_storm))){
			weather.setImageResource(R.anim.anim_weather_sand_storm);
			today_1.setImageResource(R.drawable.sand_storm03);
		} else if (weather_status.equals(getString(R.string.str_fog))){
			weather.setImageResource(R.anim.anim_weather_fog);
			today_1.setImageResource(R.drawable.fog03);
		} else if (weather_status.equals(getString(R.string.str_sand_blowing))){
			weather.setImageResource(R.anim.anim_weather_sand_blowing);
			today_1.setImageResource(R.drawable.sand_blowing03);
		} else if (weather_status.equals(getString(R.string.str_s_snow))){
			weather.setImageResource(R.anim.anim_weather_s_snow);
			today_1.setImageResource(R.drawable.s_snow03);
		} else if (weather_status.equals(getString(R.string.str_m_snow))){
			weather.setImageResource(R.anim.anim_weather_m_snow);
			today_1.setImageResource(R.drawable.m_snow03);
		} else if (weather_status.equals(getString(R.string.str_l_snow))){
			weather.setImageResource(R.anim.anim_weather_l_snow);
			today_1.setImageResource(R.drawable.l_snow03);
		} else if (weather_status.equals(getString(R.string.str_h_snow))){
			weather.setImageResource(R.anim.anim_weather_h_snow);
			today_1.setImageResource(R.drawable.h_snow03);
		} else if (weather_status.equals(getString(R.string.str_snow_shower))){
			weather.setImageResource(R.anim.anim_weather_snow_shower);
			today_1.setImageResource(R.drawable.snow_shower03);
		} else if (weather_status.equals(getString(R.string.str_s_rain))){
			weather.setImageResource(R.anim.anim_weather_s_rain);
			today_1.setImageResource(R.drawable.s_rain03);
		} else if (weather_status.equals(getString(R.string.str_m_rain))){
			weather.setImageResource(R.anim.anim_weather_m_rain);
			today_1.setImageResource(R.drawable.m_rain03);
		} else if (weather_status.equals(getString(R.string.str_l_rain))){
			weather.setImageResource(R.anim.anim_weather_l_rain);
			today_1.setImageResource(R.drawable.l_rain03);
		} else if (weather_status.equals(getString(R.string.str_h_rain))){
			weather.setImageResource(R.anim.anim_weather_h_rain);
			today_1.setImageResource(R.drawable.h_rain03);
		} else if (weather_status.equals(getString(R.string.str_hh_rain))){
			weather.setImageResource(R.anim.anim_weather_hh_rain);
			today_1.setImageResource(R.drawable.hh_rain03);
		} else if (weather_status.equals(getString(R.string.str_hhh_rain))){
			weather.setImageResource(R.anim.anim_weather_hhh_rain);
			today_1.setImageResource(R.drawable.hhh_rain03);
		} else if (weather_status.equals(getString(R.string.str_shower))){
			weather.setImageResource(R.anim.anim_weather_shower);
			today_1.setImageResource(R.drawable.shower03);
		} else if (weather_status.equals(getString(R.string.str_thunder_shower))){
			weather.setImageResource(R.anim.anim_weather_thunder_shower);
			today_1.setImageResource(R.drawable.thunder_shower03);
		} else {
			isWeatherValid = false;
		}

		if(isWeatherValid) {
			img_weather = (Drawable)today_1.getDrawable();

			if (anim_weather != null){
				anim_weather.stop();
				anim_weather = null;
			}
			anim_weather= (AnimationDrawable)weather.getDrawable();
			if (hasAnimation == true){
				anim_weather.start();
			} else {
				anim_weather.stop();
			}
		}
		
	}

	private void setTomorrowTem(){
		String[] list_weather_info = loadWeather(WEATHER_PATH_1);
		if ((list_weather_info == null) || (list_weather_info.length < 4)){
			return;
		}
		

		TextView tomorrow_2 = (TextView)findViewById(R.id.tx_1_2);
		TextView tomorrow_3 = (TextView)findViewById(R.id.tx_1_3);
		TextView tomorrow_4 = (TextView)findViewById(R.id.tx_1_4);
		tomorrow_2.setText(list_weather_info[0]);
		tomorrow_3.setText(list_weather_info[2]);
		tomorrow_4.setText(list_weather_info[3]);	

		ImageView tomorrow_1 = (ImageView)findViewById(R.id.img_1_1);
		String weather_status = list_weather_info[1];

		//Log.d(TAG, "@@@@@@@@@@@@@@  " + list_weather_info[1]);
		
		if (weather_status.equals(getString(R.string.str_cloudy))){
			tomorrow_1.setImageResource(R.drawable.cloudy03);
		} else if (weather_status.equals(getString(R.string.str_sunny))){
			tomorrow_1.setImageResource(R.drawable.sunny03);
		} else if (weather_status.equals(getString(R.string.str_smoke))){
			tomorrow_1.setImageResource(R.drawable.smoke03);
		} else if (weather_status.equals(getString(R.string.str_shade))){
			tomorrow_1.setImageResource(R.drawable.shade03);
		} else if (weather_status.equals(getString(R.string.str_h_sand_storm))){
			tomorrow_1.setImageResource(R.drawable.h_sand_storm03);
		} else if (weather_status.equals(getString(R.string.str_sand_storm))){
			tomorrow_1.setImageResource(R.drawable.sand_storm03);
		} else if (weather_status.equals(getString(R.string.str_fog))){
			tomorrow_1.setImageResource(R.drawable.fog03);
		} else if (weather_status.equals(getString(R.string.str_sand_blowing))){
			tomorrow_1.setImageResource(R.drawable.sand_blowing03);
		} else if (weather_status.equals(getString(R.string.str_s_snow))){
			tomorrow_1.setImageResource(R.drawable.s_snow03);
		} else if (weather_status.equals(getString(R.string.str_m_snow))){
			tomorrow_1.setImageResource(R.drawable.m_snow03);
		} else if (weather_status.equals(getString(R.string.str_l_snow))){
			tomorrow_1.setImageResource(R.drawable.l_snow03);
		} else if (weather_status.equals(getString(R.string.str_h_snow))){
			tomorrow_1.setImageResource(R.drawable.h_snow03);
		} else if (weather_status.equals(getString(R.string.str_snow_shower))){
			tomorrow_1.setImageResource(R.drawable.snow_shower03);
		} else if (weather_status.equals(getString(R.string.str_s_rain))){
			tomorrow_1.setImageResource(R.drawable.s_rain03);
		} else if (weather_status.equals(getString(R.string.str_m_rain))){
			tomorrow_1.setImageResource(R.drawable.m_rain03);
		} else if (weather_status.equals(getString(R.string.str_l_rain))){
			tomorrow_1.setImageResource(R.drawable.l_rain03);
		} else if (weather_status.equals(getString(R.string.str_h_rain))){
			tomorrow_1.setImageResource(R.drawable.h_rain03);
		} else if (weather_status.equals(getString(R.string.str_hh_rain))){
			tomorrow_1.setImageResource(R.drawable.hh_rain03);
		} else if (weather_status.equals(getString(R.string.str_hhh_rain))){
			tomorrow_1.setImageResource(R.drawable.hhh_rain03);
		} else if (weather_status.equals(getString(R.string.str_shower))){
			tomorrow_1.setImageResource(R.drawable.shower03);
		} else if (weather_status.equals(getString(R.string.str_thunder_shower))){
			tomorrow_1.setImageResource(R.drawable.thunder_shower03);
		}
	}

	private void setAfterTomTem(){
		String[] list_weather_info = loadWeather(WEATHER_PATH_2);
		if ((list_weather_info == null) || (list_weather_info.length < 4)){
			return;
		}
		

		TextView after_to_2 = (TextView)findViewById(R.id.tx_2_2);
		TextView after_to_3 = (TextView)findViewById(R.id.tx_2_3);
		TextView aftet_to_4 = (TextView)findViewById(R.id.tx_2_4);
		after_to_2.setText(list_weather_info[0]);
		after_to_3.setText(list_weather_info[2]);
		aftet_to_4.setText(list_weather_info[3]);	

		
		ImageView after_to_1 = (ImageView)findViewById(R.id.img_2_1);
		String weather_status = list_weather_info[1];
		//Log.d(TAG, "@@@@@@@@@@@@@@  2    " + list_weather_info[1]);
		
		if (weather_status.equals(getString(R.string.str_cloudy))){
			after_to_1.setImageResource(R.drawable.cloudy03);
		} else if (weather_status.equals(getString(R.string.str_sunny))){
			after_to_1.setImageResource(R.drawable.sunny03);
		} else if (weather_status.equals(getString(R.string.str_smoke))){
			after_to_1.setImageResource(R.drawable.smoke03);
		} else if (weather_status.equals(getString(R.string.str_shade))){
			after_to_1.setImageResource(R.drawable.shade03);
		} else if (weather_status.equals(getString(R.string.str_h_sand_storm))){
			after_to_1.setImageResource(R.drawable.h_sand_storm03);
		} else if (weather_status.equals(getString(R.string.str_sand_storm))){
			after_to_1.setImageResource(R.drawable.sand_storm03);
		} else if (weather_status.equals(getString(R.string.str_fog))){
			after_to_1.setImageResource(R.drawable.fog03);
		} else if (weather_status.equals(getString(R.string.str_sand_blowing))){
			after_to_1.setImageResource(R.drawable.sand_blowing03);
		} else if (weather_status.equals(getString(R.string.str_s_snow))){
			after_to_1.setImageResource(R.drawable.s_snow03);
		} else if (weather_status.equals(getString(R.string.str_m_snow))){
			after_to_1.setImageResource(R.drawable.m_snow03);
		} else if (weather_status.equals(getString(R.string.str_l_snow))){
			after_to_1.setImageResource(R.drawable.l_snow03);
		} else if (weather_status.equals(getString(R.string.str_h_snow))){
			after_to_1.setImageResource(R.drawable.h_snow03);
		} else if (weather_status.equals(getString(R.string.str_snow_shower))){
			after_to_1.setImageResource(R.drawable.snow_shower03);
		} else if (weather_status.equals(getString(R.string.str_s_rain))){
			after_to_1.setImageResource(R.drawable.s_rain03);
		} else if (weather_status.equals(getString(R.string.str_m_rain))){
			after_to_1.setImageResource(R.drawable.m_rain03);
		} else if (weather_status.equals(getString(R.string.str_l_rain))){
			after_to_1.setImageResource(R.drawable.l_rain03);
		} else if (weather_status.equals(getString(R.string.str_h_rain))){
			after_to_1.setImageResource(R.drawable.h_rain03);
		} else if (weather_status.equals(getString(R.string.str_hh_rain))){
			after_to_1.setImageResource(R.drawable.hh_rain03);
		} else if (weather_status.equals(getString(R.string.str_hhh_rain))){
			after_to_1.setImageResource(R.drawable.hhh_rain03);
		} else if (weather_status.equals(getString(R.string.str_shower))){
			after_to_1.setImageResource(R.drawable.shower03);
		} else if (weather_status.equals(getString(R.string.str_thunder_shower))){
			after_to_1.setImageResource(R.drawable.thunder_shower03);
		}
	}
	

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch(msg.what) {
				case UPDATE_INFO:
					displayDate();
					break;
				case UPDATE_WEATHER:
					setTodayTem();
					setTomorrowTem();
					setAfterTomTem();
					break;
				default:
					break;
			}
		}
	};

	
	class mThread extends Thread { 
		public void run() {

			Message msg = new Message();
			city = mWeather.getCity(str_province, str_city, str_area, str_come_from);
			
			if (city != null){
			
				
				mWeather.getweather(city, "0");
				temperature = mWeather.getLowTem() + str_tem + "~" + mWeather.getHighTem() + str_tem;
				weather_status = mWeather.getWeatherStatus();
				if (mWeather.isWeatherWillChange() == true){
					weather_status_detail = mWeather.getWeatherStatus() + str_dunhao + mWeather.getWeatherStatus2();
				} else {
					weather_status_detail = weather_status;
				}
				setWeatherInfo(0);
					
				mWeather.getweather(city, "1");
				setWeatherInfo(1);
				
				mWeather.getweather(city, "2");
				setWeatherInfo(2);

				mHandler.sendEmptyMessage(UPDATE_WEATHER);
			}
		}
	}

	

	
	private BroadcastReceiver mediaReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			//Log.d(TAG, " mediaReceiver        action = " + action);
			if(action == null)
				return;
		
			if(Intent.ACTION_MEDIA_EJECT.equals(action)
					|| Intent.ACTION_MEDIA_UNMOUNTED.equals(action) || Intent.ACTION_MEDIA_MOUNTED.equals(action)) {
				displayStatus();
				updateStatus();		
			}
		}
	};

	private BroadcastReceiver netReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if(action == null)
				return;

			//Log.d(TAG, "netReceiver         action = " + action);

			if (action.equals(Intent.ACTION_TIME_TICK)){
				displayDate();
				
				time_count++;
				if(time_count >= 60){
					displayWeather();
					time_count = 0;
				}
			} else {
				displayStatus();
				updateStatus();
			
				if(net_change_action.equals(action)) {
					if (isConnectingToInternet() == true){
						displayWeather();
					}
				} else if((WifiManager.WIFI_AP_STATE_CHANGED_ACTION).equals(action)) {
					int wifi_AP_State =  intent.getIntExtra(WifiManager.EXTRA_WIFI_AP_STATE, WifiManager.WIFI_AP_STATE_FAILED);
					if(WifiManager.WIFI_AP_STATE_ENABLED == wifi_AP_State)
					{
						displayWeather();
					}
				}
			}
		}
	};
	private int loadWallpaper(){
		 File mFile = new File(WALLPAPER_PATH);

		if(!mFile.exists()) {
			return -1;
		}
		
		try {
			FileReader fr = new FileReader(mFile);
			char[] buf = new char[200];
			
			int len = fr.read(buf);

			String path =  DATA_PATH + new String(buf, 0, len);
			Log.d(TAG, "wallpaper is " + path);

			if ((new File(path)).exists()){	
				bmp_bg = BitmapFactory.decodeFile(path, null); 
				if (bmp_bg != null) {
					img_bg = new BitmapDrawable(bmp_bg);
				} else {
					Log.d(TAG, "wallpaper is null");
					return -1;				
				}
			}else{
				return -1;
			}
		
			fr.close();
		}
		catch (Exception e) {
			return -1;
		}
		
		return 0;
	}

	public static int loadFactoryWallpaper(){
		 File mFile = new File(FACTORY_WALLPAPER_PATH);

		if(!mFile.exists()) {
			return -1;
		}
		
		try {

			bmp_bg = BitmapFactory.decodeFile(FACTORY_WALLPAPER_PATH, null); 
			if (bmp_bg != null) {
				img_bg = new BitmapDrawable(bmp_bg);
			} else {
				Log.d(TAG, "factory wallpaper is null");
				return -1;				
			}
		}
		catch (Exception e) {
			return -1;
		}
		
		return 0;
	}
	
	private void setWeatherInfo(int day){
		if (day == 0) {
			    String weather_info = null;
				weather_info = city + ";";
				weather_info = weather_info + temperature + ";";
				weather_info = weather_info + weather_status +  ";";
				weather_info = weather_info + weather_status_detail +  ";";
				weather_info = weather_info + mWeather.getWindDirection() + ";";
				saveWeather(WEATHER_PATH_0, weather_info);	
				weather_info = null;
		} else {
				String weather_info = null;
				weather_info =  mWeather.getLowTem() + str_tem + "~" + mWeather.getHighTem() + str_tem + ";";
				weather_info +=  mWeather.getWeatherStatus() + ";";
				if (mWeather.isWeatherWillChange() == true){
					weather_info += mWeather.getWeatherStatus() + str_dunhao + mWeather.getWeatherStatus2() + ";";
				} else {
					weather_info += mWeather.getWeatherStatus()+ ";";
				}
				weather_info += mWeather.getWindDirection() + ";";	
				
				if (day == 1){
						saveWeather(WEATHER_PATH_1, weather_info);	
				}else{
						saveWeather(WEATHER_PATH_2, weather_info);	
				}
		}		


	}

	private boolean saveWeather(String weather_path,String str_weather_info) {
		File mFile = new File(weather_path);
		if(!mFile.exists()) {
			try {
				mFile.createNewFile();
			}
			catch (Exception e) {
				Log.e(TAG, e.getMessage().toString());
				return false;
			}
		}
		Log .d(TAG, "  " + weather_path + "     "+ str_weather_info);
		try {
			FileWriter fw = new FileWriter(mFile);
			fw.write(str_weather_info);
			fw.close();
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}
	
	private String[] loadWeather(String weather_path){
		 String[] list;
		 File mFile = new File(weather_path);

		if(!mFile.exists()) {
			return null;
		}
		
		try {
			FileReader fr = new FileReader(mFile);
			char[] buf = new char[5120];
			
			int len = fr.read(buf);

			String str = new String(buf, 0, len);
			Log.d(TAG, "load string is " + str);

			list = str.split(";");

			fr.close();
			return list;
		}
		catch (Exception e) {
			return null;
		}

	}

	public static View.OnKeyListener myOnKeylistener = new  View.OnKeyListener(){
        public boolean onKey (View v, int keyCode, KeyEvent event){
			if (event.getAction() == KeyEvent.ACTION_DOWN && isSystemSoundOn == true){			
				//Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@ presskey: " + keyCode);
				
				if (keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_DPAD_DOWN 
					|| keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT){
					sp_foucs_change.stop(music_prio_focus);
					sp_foucs_change.play(music_prio_focus, 1, 1, 0, 0, 1);
				} else if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER){
					sp_button.stop(music_prio_button);
					sp_button.play(music_prio_button, 1, 1, 0, 0, 1);
				}				
			}
			return false;
		}
	};

	public static void playClickMusic() {	
		if (isSystemSoundOn == true) {
			sp_button.stop(music_prio_button);
			sp_button.play(music_prio_button, 1, 1, 0, 0, 1);
		}		
	}
	
	public static void ReleaseMusic() {	
		sp_foucs_change.release();
		sp_button.release();							
	}

	private boolean isSystemSoundEnabled() {
		ContentResolver resolver = getContentResolver();
	
		if (Settings.System.getInt(resolver,Settings.System.SOUND_EFFECTS_ENABLED, 1) == 1)
			return true;
		else 
			return false;
	}
	
}
