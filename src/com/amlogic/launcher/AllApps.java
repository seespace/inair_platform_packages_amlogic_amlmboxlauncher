package com.amlogic.launcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.os.SystemProperties;
import android.util.Log;
import android.view.View;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import android.widget.BaseAdapter;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;

import android.text.format.DateFormat;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;

import java.util.Map;
import java.lang.Runnable;


public class AllApps extends Activity {
	private static final String TAG = "AllApps";

	private GridView mGrid;
	private GridView lv_status = null;
	private GridView lv_menu = null;

	private ArrayList<ApplicationInfo> mApplications;
	private ApplicationsAdapter mAppAdapter;
	private AppReceiver mAppReceiver;

	private  AnimationDrawable anim_selector_1 = null;
	private  AnimationDrawable anim_selector_2 = null;

	private TextView tx_apps = null;
	
	private boolean into_mate_activity = false;
	private RelativeLayout layout_main = null; 

	private Drawable mDrawable = null;
	private Bitmap mBmp = null;
	private boolean into_home = false;
	private final String HIDE_STATUSBAR = "sys.hideStatusBar.enable";
	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);  
		setContentView(R.layout.main_apps); 

				
		layout_main = (RelativeLayout)findViewById(R.id.layout_main);
		if (Launcher.img_bg != null){
			mDrawable = Launcher.img_bg;
			mBmp = Launcher.bmp_bg;
			layout_main.setBackgroundDrawable(Launcher.img_bg);		
		}

		SystemProperties.set(HIDE_STATUSBAR,"true");
		
		tx_apps = (TextView)findViewById(R.id.tx_apps);
		tx_apps.setBackgroundResource(R.drawable.bg_menu_tx);

		lv_status = (GridView)findViewById(R.id.list_status);
		

		
        mGrid = (GridView) findViewById(R.id.grid_apps);
		anim_selector_1 = (AnimationDrawable)mGrid.getSelector();
		
		loadApplications();
	    bindApplications();
	    mGrid.setOnItemClickListener(new ApplicationLauncher());

		lv_menu = (GridView)findViewById(R.id.list_menu);
		anim_selector_2 = (AnimationDrawable)lv_menu.getSelector();
		lv_menu.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(pos);
				Intent intent = new Intent();

				Launcher.playClickMusic();

				//into_mate_activity = true;	
		            if(item.get("item_type").equals(R.drawable.btn_home)) {
						into_home = true;
					  	intent.setClass(AllApps.this, Launcher.class);
					  	startActivity(intent);
		            }else if(item.get("item_type").equals(R.drawable.btn_local)){
						intent.setClass(AllApps.this, LocalActivity.class);
						startActivity(intent);
					}else if(item.get("item_type").equals(R.drawable.btn_browser)){
						intent.setClass(AllApps.this, BrowserActivity.class);
						startActivity(intent);
					}else if(item.get("item_type").equals(R.drawable.btn_setting)){
						intent .setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
						startActivity(intent);
					}   
			     
            }
        });
		
		mGrid.setOnKeyListener(Launcher.myOnKeylistener);
		lv_menu.setOnKeyListener(Launcher.myOnKeylistener);

		animStart();
		displayStatus();
		displayMenu();
		 
	     mAppReceiver = new AppReceiver();
	     // Register intent receivers
	     IntentFilter filter = new IntentFilter(Intent.ACTION_PACKAGE_ADDED);
	     filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
	     filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
		 filter.addDataScheme("package");
	     registerReceiver(mAppReceiver, filter);

	     

	}
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "------onResume");

		SystemProperties.set(HIDE_STATUSBAR,"true");
		into_mate_activity = false;
        
        if(SystemProperties.getBoolean("ro.platform.has.mbxuimode", false)){
            if(SystemProperties.getBoolean("ubootenv.var.has.accelerometer", true)
                            && SystemProperties.getBoolean("sys.keeplauncher.landcape", false))
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            else
               setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
         }
		
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_MEDIA_EJECT);
		filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
		filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
		filter.addDataScheme("file");
		registerReceiver(mediaReceiver, filter);

		filter = new IntentFilter();
		filter.addAction(Launcher.net_change_action);
		filter.addAction(Launcher.wifi_signal_action);
		filter.addAction(Intent.ACTION_TIME_TICK);	
		registerReceiver(netReceiver, filter);


		displayDate(); 

		animStart();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "------onPause");
		if(!into_mate_activity) {
				SystemProperties.set(HIDE_STATUSBAR,"false");
				into_mate_activity = false;
		}

		unregisterReceiver(mediaReceiver);
		unregisterReceiver(netReceiver);
 
		animStop();
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "------onStop");

		if (into_home == true){	
			if (mDrawable != null){
				mDrawable.setCallback(null);
			}	
			if (mBmp != null){
				mBmp.recycle();
			}
			into_home = false;
		}
		
	}

	protected void onDestroy(){
	     unregisterReceiver(mAppReceiver);
	     super.onDestroy();
	}


	public boolean onKeyDown(int keyCode, KeyEvent event) {
	  if(keyCode == KeyEvent.KEYCODE_BACK) {
	  	 Intent intent = new Intent();
		 intent.setClass(AllApps.this, Launcher.class);
		 startActivity(intent);
		 
		 return true;
	  }
      
     if(keyCode == KeyEvent.KEYCODE_SEARCH) {        
          SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
          ComponentName globalSearchActivity = searchManager.getGlobalSearchActivity();
          if (globalSearchActivity == null) {
              Log.w(TAG, "No global search activity found.");
             return false;
         }
          
         Intent intent = new Intent(SearchManager.INTENT_ACTION_GLOBAL_SEARCH);
         intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         intent.setComponent(globalSearchActivity);
         Bundle appSearchData = new Bundle();
         appSearchData.putString("source", "launcher-search");
         intent.putExtra(SearchManager.APP_DATA, appSearchData);
         startActivity(intent);
		 return true;
	  }
		 return super.onKeyDown(keyCode, event);
	}
	 
		
	private  void animStart(){
		if (Launcher.hasAnimation == true){
			anim_selector_1.start();
			anim_selector_2.start();
		} else {
			anim_selector_1.stop();
			anim_selector_2.stop();
		}
	}
	private  void animStop(){
		anim_selector_1.stop();
		anim_selector_2.stop();
		anim_selector_1.setCallback(null);
		anim_selector_2.setCallback(null);
	}

	private void displayStatus() {
		WifiManager mWifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
        WifiInfo mWifiInfo = mWifiManager.getConnectionInfo();
        int wifi_rssi = mWifiInfo.getRssi();
		int wifi_level = WifiManager.calculateSignalLevel(
                        wifi_rssi, 5);
	
	 
		 LocalAdapter ad = new LocalAdapter(AllApps.this,
						 Launcher.getStatusData(wifi_level, isEthernetOn()),
						 R.layout.homelist_item, 			 
									 new String[] {"item_type", "item_name", "item_sel"},
									 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
		 lv_status.setAdapter(ad);
	}

	private void updateStatus() {
		   ((BaseAdapter) lv_status.getAdapter()).notifyDataSetChanged();
	}

	private void displayDate() {
			int[] array = Launcher.getTime();
			
			ImageView hour_1 = (ImageView)findViewById(R.id.img_hour_1);	
			ImageView hour_2 = (ImageView)findViewById(R.id.img_hour_2);
			ImageView colon = (ImageView)findViewById(R.id.img_colon);
			ImageView minu_1 = (ImageView)findViewById(R.id.img_minu_1);
			ImageView minu_2 = (ImageView)findViewById(R.id.img_minu_2);	
			ImageView day = (ImageView)findViewById(R.id.img_day);
	
			BrowserActivity.setImage(hour_1, array[0]);
			BrowserActivity.setImage(hour_2, array[1]);
			colon.setImageResource(R.drawable.s_colon);
			BrowserActivity.setImage(minu_1, array[2]);
			BrowserActivity.setImage(minu_2, array[3]);
	
			Launcher.is24hFormart = DateFormat.is24HourFormat(this); 
			if (array[4] != -1) {
				day.setVisibility(View.VISIBLE);
				if (array[4] == 0) {
					day.setImageResource(R.drawable.s_am);
				} else{
					day.setImageResource(R.drawable.s_pm);
				}
			} else {
				day.setVisibility(View.GONE);
			}
		
			

			ImageView weather = (ImageView)findViewById(R.id.img_weather);
			TextView  temperature = (TextView)findViewById(R.id.tx_temperature);
			
			weather.setImageDrawable(Launcher.img_weather);
			temperature.setText(Launcher.temperature);
			temperature.setTypeface(Typeface.MONOSPACE,Typeface.BOLD_ITALIC);
			temperature.setTextSize(20);
	}
	
	private void displayMenu() {
	
	
			 LocalAdapter ad = new LocalAdapter(AllApps.this,
							 Launcher.getMenuData(),
							 R.layout.list_menu,			 
										 new String[] {"item_type", "item_name", "item_sel"},
										 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
			 lv_menu.setAdapter(ad);
			 lv_menu.setSelection(1);
			 lv_menu.requestFocus();

	}

	private boolean isEthernetOn(){
		ConnectivityManager connectivity = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
	
		if (info.isConnected()){
			//Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ true");
			return true;
		} else {
		    	//Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ false");
			return false;
		}
	}
 
    private class ApplicationLauncher implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView parent, View v, int position, long id) {
            ApplicationInfo app = (ApplicationInfo) parent.getItemAtPosition(position);

			Launcher.playClickMusic();
            startActivity(app.intent);
	}
    }

	private void loadApplications() {
//        if (mApplications != null) {
//            return;
//        }

        PackageManager manager = getPackageManager();

        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        final List<ResolveInfo> apps = manager.queryIntentActivities(mainIntent, 0);
        Collections.sort(apps, new ResolveInfo.DisplayNameComparator(manager));

        if (apps != null) {
            final int count = apps.size();

            if (mApplications == null) {
                mApplications = new ArrayList<ApplicationInfo>(count);
            }
            mApplications.clear();

            for (int i = 0; i < count; i++) {
                ApplicationInfo application = new ApplicationInfo();
                ResolveInfo info = apps.get(i);

                application.title = info.loadLabel(manager);
                application.setActivity(new ComponentName(
                        info.activityInfo.applicationInfo.packageName,
                        info.activityInfo.name),
                        Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                application.icon = info.activityInfo.loadIcon(manager);
                
                Log.i("AllApps3D", ""+application.title.toString());
                
                mApplications.add(application);
            }
        }
    }
	
    /**
     * Creates a new appplications adapter for the grid view and registers it.
     */
    private void bindApplications() {
    	mAppAdapter = new ApplicationsAdapter(this, mApplications);
   
        Log.i("TAG", ""+mApplications.size());
        mGrid.setAdapter(mAppAdapter);
    } 
	 
	
	private Handler mHandler = new Handler() {
			public void handleMessage(Message msg) {
				switch(msg.what) {
					case Launcher.UPDATE_INFO:
						displayDate();
						break;
					default:
						break;
				}
			}
	};
    
    private class AppReceiver extends BroadcastReceiver{
    	
    	private static final String TAG = "AppReceiver";
        
    	@Override
    	public void onReceive(Context context, Intent intent) {
    		// TODO Auto-generated method stub
    		Log.i("AppReceiver", "onReceive");
           
                final String action = intent.getAction();

                if (Intent.ACTION_PACKAGE_CHANGED.equals(action)
                        || Intent.ACTION_PACKAGE_REMOVED.equals(action)
                        || Intent.ACTION_PACKAGE_ADDED.equals(action)) {
                	
                	Log.i("AppReceiver", "Intent");
                	
                    final String packageName = intent.getData().getSchemeSpecificPart();
                    final boolean replacing = intent.getBooleanExtra(Intent.EXTRA_REPLACING, false);

                    if (packageName == null || packageName.length() == 0) {
                        // they sent us a bad intent
                        return;
                    }

                    if (Intent.ACTION_PACKAGE_CHANGED.equals(action)) {
                    	Log.i("AppReceiver", "ACTION_PACKAGE_CHANGED");

                		loadApplications();
                	    bindApplications();

                    } else if (Intent.ACTION_PACKAGE_REMOVED.equals(action)) {
                    	Log.i("AppReceiver", "ACTION_PACKAGE_REMOVED");

                		loadApplications();
                	    bindApplications();

                        // else, we are replacing the package, so a PACKAGE_ADDED will be sent
                        // later, we will update the package at this time
                    } else if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
                    	Log.i("AppReceiver", "ACTION_PACKAGE_ADDED");

                		loadApplications();
                	    bindApplications();

                    }
				}else	if((WifiManager.WIFI_AP_STATE_CHANGED_ACTION).equals(action) || Intent.ACTION_MEDIA_EJECT.equals(action)
									|| Intent.ACTION_MEDIA_UNMOUNTED.equals(action) || Intent.ACTION_MEDIA_MOUNTED.equals(action)) {
						displayStatus();
						updateStatus();			
	     		}
                    

    	}	
	}
	
	private BroadcastReceiver mediaReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			//Log.d(TAG, " mediaReceiver        action = " + action);
			if(action == null)
				return;
		
			if(Intent.ACTION_MEDIA_EJECT.equals(action)
					|| Intent.ACTION_MEDIA_UNMOUNTED.equals(action) || Intent.ACTION_MEDIA_MOUNTED.equals(action)) {
				displayStatus();
				updateStatus();		
			}
		}
	};

	private BroadcastReceiver netReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if(action == null)
				return;

			//Log.d(TAG, "netReceiver         action = " + action);

			if (action.equals(Intent.ACTION_TIME_TICK)){
				displayDate();		
			} else {
				displayStatus();
				updateStatus();
			}
		}
	};

	
}
