package com.amlogic.launcher;

import android.app.Activity;
import android.content.Intent;
import android.content.ComponentName;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.SystemProperties;


import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;


import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.GridView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;


import android.graphics.drawable.Drawable;
import android.graphics.drawable.AnimationDrawable;
import android.util.Log;


import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;



public class CustomAppsActivity extends Activity {
    /** Called when the activity is first created. */
	private static final String TAG = "CustomAppsActivity";
	
	private ImageButton btn_home = null;
	private ImageView img_bar = null;
	private TextView tx_home = null;
	private GridView gv = null;

	private  AnimationDrawable anim_selector = null;

	private File mFile;
	private String[] list_custom_apps;
	private String str_custom_apps;

	private boolean into_home = false;
	
	private final static String CUSTOM_APPS_PATH = "/data/data/com.amlogic.launcher/custom_apps.cfg";
	private final String HIDE_STATUSBAR = "sys.hideStatusBar.enable";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	 	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);  
		getWindow().setWindowAnimations(R.style.dialogWindowAnim);
        setContentView(R.layout.layout_custom_apps);

		LayoutParams params = getWindow().getAttributes();         
        params.y =  -30;               
		getWindow().setAttributes(params);

		SystemProperties.set(HIDE_STATUSBAR,"true");
		

		gv = (GridView)findViewById(R.id.grid_add_apps);
		anim_selector = (AnimationDrawable)gv.getSelector();
		animStart();
		
		gv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(pos);

				Launcher.playClickMusic();

				if(item.get("item_type").equals(R.drawable.item_img_exit)) {
					finish();
					into_home = true;
				} else if(item.get("item_sel").equals(R.drawable.item_img_unsel)) {
						String str_package_name = ((ComponentName)item.get("item_symbol")).getPackageName() + ";";
						if (str_custom_apps == null){
							str_custom_apps = str_package_name;
						} else {
							str_custom_apps = str_custom_apps + str_package_name;
						}
						((Map<String, Object>)parent.getItemAtPosition(pos)).put("item_sel", R.drawable.item_img_sel);
						updateView();
             	} else {
						String str_package_name = ((ComponentName)item.get("item_symbol")).getPackageName() + ";";
						str_custom_apps = str_custom_apps.replaceAll(str_package_name,"");
						((Map<String, Object>)parent.getItemAtPosition(pos)).put("item_sel", R.drawable.item_img_unsel);
						updateView();
			  	}
          
            }
        });
		gv.setOnKeyListener(Launcher.myOnKeylistener);
		
		displayView();
		
    }

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "------onResume");
		SystemProperties.set(HIDE_STATUSBAR,"true");

		animStart();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "------onPause");
		
		if(!into_home) {
			SystemProperties.set(HIDE_STATUSBAR,"false");
			into_home = false;
		}
		
		saveApps();
		animStop();
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "------onStop");
	
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "------onDestroy");
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
	  if(keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			into_home = true;
			return true;
	  }
		 return super.onKeyDown(keyCode, event);
	}
	 
	 private  void animStart(){
		 if (Launcher.hasAnimation == true){
			 anim_selector.start();
		 } else {
			 anim_selector.stop();
		 }
	 }

	private  void animStop(){
		anim_selector.stop();
		anim_selector.setCallback(null);
	}

	private void displayView() {
	 
		 LocalAdapter ad = new LocalAdapter(CustomAppsActivity.this,
						 loadApplications(),
						 R.layout.add_apps_grid_item, 			 
									 new String[] {"item_type", "item_name", "item_sel"},
									 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
		 gv.setAdapter(ad);
		 gv.requestFocus();
	 }
	
	private void updateView() {
		 ((BaseAdapter) gv.getAdapter()).notifyDataSetChanged();
	}

	private List<Map<String, Object>> loadApplications() {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();	
		Map<String, Object> map = new HashMap<String, Object>();

        PackageManager manager = getPackageManager();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        final List<ResolveInfo> apps = manager.queryIntentActivities(mainIntent, 0);
        Collections.sort(apps, new ResolveInfo.DisplayNameComparator(manager));

		list_custom_apps = loadCustomApps();

		map = new HashMap<String, Object>(); 
		map.put("item_name",getString(R.string.str_exit));   
		map.put("file_path", null); 	
		map.put("item_type", R.drawable.item_img_exit);
		map.put("item_sel", R.drawable.item_img_unsel);	
		list.add(map);

        if (apps != null) {
            final int count = apps.size();

            for (int i = 0; i < count; i++) {
                ApplicationInfo application = new ApplicationInfo();
                ResolveInfo info = apps.get(i);
		
                application.title = info.loadLabel(manager);
                application.setActivity(new ComponentName(
                        info.activityInfo.applicationInfo.packageName,
                        info.activityInfo.name),
                        Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                application.icon = info.activityInfo.loadIcon(manager);
				
				map = new HashMap<String, Object>(); 
				map.put("item_name", application.title.toString());   
				map.put("file_path", application.intent); 	
				map.put("item_type", application.icon);
			 	map.put("item_sel", R.drawable.item_img_unsel);	
				map.put("item_symbol", application.componentName);

				if(list_custom_apps != null){
					for (int j=0; j < list_custom_apps.length; j++){
					
						if (application.componentName.getPackageName().equals(list_custom_apps[j])){
							 map.put("item_sel", R.drawable.item_img_sel);
							 break;
						} 
					}
				}
				list.add(map);
				// Log.i(TAG, ""+application.title.toString());  
			}
                
              

            
        }

		return list;
    }

	private String[] loadCustomApps(){
		 String[] list_custom_apps;
			
		 mFile = new File(CUSTOM_APPS_PATH);

		if(!mFile.exists()) {
				return null;
		}
		
		try {
			FileReader fr = new FileReader(mFile);
			char[] buf = new char[5120];
			
			int len = fr.read(buf);

			str_custom_apps = new String(buf, 0, len);
			Log.d(TAG, "load string is " + str_custom_apps);

			list_custom_apps = str_custom_apps.split(";");
		
			fr.close();
		}
		catch (Exception e) {
			return null;
		}
		return list_custom_apps;

	}
	
	public boolean saveApps() {
		mFile = new File(CUSTOM_APPS_PATH);
		if(!mFile.exists()) {
			try {
				mFile.createNewFile();
			}
			catch (Exception e) {
				Log.e(TAG, e.getMessage().toString());
				return false;
			}
		}
		//Debug.d(TAG, "save string is " + s_string);
		try {
			FileWriter fw = new FileWriter(mFile);
			//Log.d(TAG, "   str_custom_apps =@" + str_custom_apps + "@");
			
			if (str_custom_apps.length() == 0){
				fw.flush();
			} else {
				fw.write(str_custom_apps);
			}
			fw.flush();
			fw.close();
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}
}
