package com.amlogic.launcher;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.os.SystemProperties;
import android.os.Handler;
import android.os.Message;


import android.view.View;
import android.view.KeyEvent;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;  

import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.GridView;
import android.widget.EditText;
import android.widget.TextView.OnEditorActionListener;  
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.RelativeLayout;

import android.text.format.DateFormat;

import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.provider.Browser;
import android.database.Cursor;

import android.graphics.Typeface;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;

import android.util.Log;

import java.lang.Runnable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;


public class BrowserActivity extends Activity implements OnEditorActionListener{
    /** Called when the activity is first created. */
	private static final String TAG = "BrowserActivity";
	
	private ImageButton btn_home = null;
	private TextView tx_home = null;
	private EditText edt_search = null;
	private Button btn_search = null;

	private GridView gv = null;
	private GridView lv_status = null;
	private GridView lv_menu = null;

	private  AnimationDrawable anim_selector_1 = null;
	private  AnimationDrawable anim_selector_2 = null;

	private boolean into_mate_activity = false;
	private RelativeLayout layout_main = null;

	private final int UPDATE_BOOKMARK = 0x13646464;

	private Context context = null;
	private LocalAdapter adapter = null;

	private Drawable mDrawable = null;
	private Bitmap mBmp = null;
	private boolean into_home = false;
	private boolean into_browser = true;
	private List<Map<String, Object>> mList = null;
	private final String HIDE_STATUSBAR = "sys.hideStatusBar.enable";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	 	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);  
        setContentView(R.layout.main_browser);

		context = this.context;
				
		layout_main = (RelativeLayout)findViewById(R.id.layout_main);
		if (Launcher.img_bg != null){
			mDrawable = Launcher.img_bg;
			mBmp = Launcher.bmp_bg;
			layout_main.setBackgroundDrawable(Launcher.img_bg);		
		}

		SystemProperties.set(HIDE_STATUSBAR ,"true");

		lv_status = (GridView)findViewById(R.id.list_status);

		tx_home = (TextView)findViewById(R.id.tx_browser);
		tx_home.setBackgroundResource(R.drawable.bg_menu_tx);
	
		edt_search = (EditText)findViewById(R.id.edt_search);
        edt_search.setOnEditorActionListener(this);
		btn_search = (Button)findViewById(R.id.btn_search);
		btn_search.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				 try {
					/*Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
					String term = edt_search.getText().toString();
					if (term!=null && term.length()!=0 ){
						intent.putExtra(SearchManager.QUERY, term);
						startActivity(intent);
					}*/

					String url = edt_search.getText().toString();
					if (url!=null && url.length()!=0 ){
						 Intent it = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	      				 it.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
	        			 startActivity(it);
					}	
				 } catch (Exception e) {
					// TODO: handle exception
				}
			}
		});

		gv = (GridView)findViewById(R.id.grid_browser);
		anim_selector_1 = (AnimationDrawable)gv.getSelector();
		gv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(pos);

				Launcher.playClickMusic();

				String uri = (String)item.get("file_path");
				if (uri != null) {				
					Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
					startActivity(urlIntent);
				}
            }
        });

		lv_menu = (GridView)findViewById(R.id.list_menu);
		anim_selector_2 = (AnimationDrawable)lv_menu.getSelector();
		lv_menu.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(pos);
				Intent intent = new Intent();

				Launcher.playClickMusic();
				
				//into_mate_activity = true;	
		            if(item.get("item_type").equals(R.drawable.btn_home)) {
						into_home = true;
						into_browser = false;
					  	intent.setClass(BrowserActivity.this, Launcher.class);
					  	startActivity(intent);
		            }else if(item.get("item_type").equals(R.drawable.btn_apps)){
		            	into_browser = false;
						intent.setClass(BrowserActivity.this, AllApps.class);
						startActivity(intent);
					}else if(item.get("item_type").equals(R.drawable.btn_local)){
						into_browser = false;
						intent.setClass(BrowserActivity.this, LocalActivity.class);
						startActivity(intent);
					}else if(item.get("item_type").equals(R.drawable.btn_setting)){
						intent .setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
						startActivity(intent);
					}else if(item.get("item_type").equals(R.drawable.btn_browser)){
						intent .setComponent(new ComponentName("com.android.browser", "com.android.browser.BrowserActivity"));
						startActivity(intent);
					}    
			     
            }
        });

		gv.setOnKeyListener(Launcher.myOnKeylistener);
		lv_menu.setOnKeyListener(Launcher.myOnKeylistener);
		
    	animStart();
		displayStatus();
		displayMenu();
		
		Thread th = new mThread();
		th.start();
    }

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "------onResume");
		SystemProperties.set(HIDE_STATUSBAR ,"true");
		into_mate_activity = false;

		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_MEDIA_EJECT);
		filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
		filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
		filter.addDataScheme("file");
		registerReceiver(mediaReceiver, filter);

		filter = new IntentFilter();
		filter.addAction(Launcher.net_change_action);
		filter.addAction(Launcher.wifi_signal_action);
		filter.addAction(Intent.ACTION_TIME_TICK);	
		registerReceiver(netReceiver, filter);


		displayDate(); 
		animStart();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "------onPause");
		if(!into_mate_activity) {
				SystemProperties.set(HIDE_STATUSBAR,"false");
				into_mate_activity = false;
		}
 
		
		unregisterReceiver(mediaReceiver);
		unregisterReceiver(netReceiver);
		animStop();
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "------onStop");

		if (into_browser != true) {
			Thread th = new recycleThread();
			th.start();
			into_browser = true;
		} 
		
		if (into_home == true){	
			if (mDrawable != null){
				mDrawable.setCallback(null);
			}	
			if (mBmp != null){
				mBmp.recycle();
			}
			into_home = false;
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "------onDestroy");
	}

	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	  //Log.d(TAG, "@@@@@@@@@@@@@@@@@@@@@@@@@ " + keyCode);
		
	  if(keyCode == KeyEvent.KEYCODE_BACK) {
	  	 Intent intent = new Intent();
		 intent.setClass(BrowserActivity.this, Launcher.class);
		 startActivity(intent);
		 
		 return true;
	  }
		 return super.onKeyDown(keyCode, event);
	}

    @Override  
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {  
        if (actionId == EditorInfo.IME_ACTION_GO) {
            String url = edt_search.getText().toString();
			if (url!=null && url.length()!=0 ){
			    Intent it = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	      		it.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
	            startActivity(it);
			}	
        }
 
        return true;  
    }  

	private  void animStart(){
		if (Launcher.hasAnimation == true){
			anim_selector_1.start();
			anim_selector_2.start();
		} else {
			anim_selector_1.stop();
			anim_selector_2.stop();
		}
	}
	private  void animStop(){
		anim_selector_1.stop();
		anim_selector_2.stop();
		anim_selector_1.setCallback(null);
		anim_selector_2.setCallback(null);
	}

	private void displayStatus() {
		WifiManager mWifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
        WifiInfo mWifiInfo = mWifiManager.getConnectionInfo();
        int wifi_rssi = mWifiInfo.getRssi();
		int wifi_level = WifiManager.calculateSignalLevel(
                        wifi_rssi, 5);
	
	 
		 LocalAdapter ad = new LocalAdapter(BrowserActivity.this,
						 Launcher.getStatusData(wifi_level, isEthernetOn()),
						 R.layout.homelist_item, 			 
									 new String[] {"item_type", "item_name", "item_sel"},
									 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
		 lv_status.setAdapter(ad);
	}

	private void updateStatus() {
		   ((BaseAdapter) lv_status.getAdapter()).notifyDataSetChanged();
	}

	private void displayDate() {
			int[] array = Launcher.getTime();
			
			ImageView hour_1 = (ImageView)findViewById(R.id.img_hour_1);	
			ImageView hour_2 = (ImageView)findViewById(R.id.img_hour_2);
			ImageView colon = (ImageView)findViewById(R.id.img_colon);
			ImageView minu_1 = (ImageView)findViewById(R.id.img_minu_1);
			ImageView minu_2 = (ImageView)findViewById(R.id.img_minu_2);	
			ImageView day = (ImageView)findViewById(R.id.img_day);
	
			setImage(hour_1, array[0]);
			setImage(hour_2, array[1]);
			colon.setImageResource(R.drawable.s_colon);
			setImage(minu_1, array[2]);
			setImage(minu_2, array[3]);

			Launcher.is24hFormart = DateFormat.is24HourFormat(this); 
			if (array[4] != -1) {
				day.setVisibility(View.VISIBLE);
				if (array[4] == 0) {
					day.setImageResource(R.drawable.s_am);
				} else{
					day.setImageResource(R.drawable.s_pm);
				}
			} else {
				day.setVisibility(View.GONE);
			}
		
	

			ImageView weather = (ImageView)findViewById(R.id.img_weather);
			TextView  temperature = (TextView)findViewById(R.id.tx_temperature);
			
			weather.setImageDrawable(Launcher.img_weather);
			temperature.setText(Launcher.temperature);
			temperature.setTypeface(Typeface.MONOSPACE,Typeface.BOLD_ITALIC);
			temperature.setTextSize(20);
	
	}
		
	public static void setImage(Object data, int i){
				ImageView img = (ImageView)data;
		
				switch(i){
					case 0:
						img.setImageResource(R.drawable.img_s_0);
						break;
					case 1:
						img.setImageResource(R.drawable.img_s_1);
						break;
					case 2:
						img.setImageResource(R.drawable.img_s_2);
						break;
					case 3:
						img.setImageResource(R.drawable.img_s_3);
						break;
					case 4:
						img.setImageResource(R.drawable.img_s_4);
						break;
					case 5:
						img.setImageResource(R.drawable.img_s_5);
						break;
					case 6:
						img.setImageResource(R.drawable.img_s_6);
						break;
					case 7:
						img.setImageResource(R.drawable.img_s_7);
						break;
					case 8:
						img.setImageResource(R.drawable.img_s_8);
						break;
					case 9:
						img.setImageResource(R.drawable.img_s_9);
						break;	
					default:
						break;
				}
		
			} 


	private void displayMenu() {
	
	
			 LocalAdapter ad = new LocalAdapter(BrowserActivity.this,
							 Launcher.getMenuData(),
							 R.layout.list_menu,			 
										 new String[] {"item_type", "item_name", "item_sel"},
										 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
			 lv_menu.setAdapter(ad);
			 lv_menu.setSelection(3);
			 lv_menu.requestFocus();
	}
	
	private void displayView() {
	 
		/* LocalAdapter ad = new LocalAdapter(BrowserActivity.this,
						 getBookMarkData(),
						 R.layout.browsergrid_item,				 
									 new String[] {"item_type", "item_name", "item_sel"},
									 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});*/
		 gv.setAdapter(adapter);
	 }

	private boolean isEthernetOn(){
		ConnectivityManager connectivity = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
	
		if (info.isConnected()){
			return true;
		} else {
			return false;
		}
	}
	 
    private List<Map<String, Object>> getBookMarkData() {
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();	
		Map<String, Object> map = new HashMap<String, Object>();

		ContentResolver contentResolver = getContentResolver();  
		String orderBy = Browser.BookmarkColumns.VISITS + " DESC";  
        String whereClause = Browser.BookmarkColumns.BOOKMARK + " = 1";  
        Cursor cursor = contentResolver.query(Browser.BOOKMARKS_URI, Browser.HISTORY_PROJECTION, whereClause, null, orderBy);  

		while(cursor!=null && cursor.moveToNext()){ 
			map = new HashMap<String, Object>(); 
            map.put("item_name", cursor.getString(cursor.getColumnIndex(Browser.BookmarkColumns.TITLE)));  
           	map.put("file_path",cursor.getString(cursor.getColumnIndex(Browser.BookmarkColumns.URL)));  
			map.put("item_sel", R.drawable.item_img_unsel);
           byte[] b = cursor.getBlob(cursor.getColumnIndex(Browser.BookmarkColumns.THUMBNAIL)); 
		  // BitmapFactory.Options options = new BitmapFactory.Options();  
    	  // options.inSampleSize = 2;
	   
           if(b!=null){  
               map.put("item_type", BitmapFactory.decodeByteArray(b, 0, b.length));  
           }
		   else{  
               map.put("item_type", ((BitmapDrawable)(getResources().getDrawable(R.drawable.launcher_browser))).getBitmap());  
           }  
		   	list.add(map);
       }  
		cursor.close();
		
		mList = list;
	    return list;
	}
	
 

	class mThread extends Thread { 
		public void run() {

			adapter = new LocalAdapter(BrowserActivity.this,
						 getBookMarkData(),
						 R.layout.browsergrid_item,				 
									 new String[] {"item_type", "item_name", "item_sel"},
									 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});

			Message msg = new Message();
			msg.what = UPDATE_BOOKMARK;
			mHandler.sendMessage(msg);	
		}
	}

	class recycleThread extends Thread { 
		public void run() {
			for (int i = 0; i < mList.size(); i++){
				Map<String, Object> item = (Map<String, Object>)mList.get(i);
				final Object data = item.get("item_type");

				if(data != null && (data instanceof Bitmap)) {
					((Bitmap)data).recycle();
				} else {
					((Drawable)data).setCallback(null);
				}
			}
		}
	}

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch(msg.what) {
				case Launcher.UPDATE_INFO:
					displayDate();
					break;
				case UPDATE_BOOKMARK:
					displayView();
					break;
				default:
					break;
			}
		}
	};

	
	private BroadcastReceiver mediaReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
	
				//Log.d(TAG, " mediaReceiver		  action = " + action);
				if(action == null)
					return;
			
				if(Intent.ACTION_MEDIA_EJECT.equals(action)
						|| Intent.ACTION_MEDIA_UNMOUNTED.equals(action) || Intent.ACTION_MEDIA_MOUNTED.equals(action)) {
					displayStatus();
					updateStatus(); 	
				}
			}
		};
	
	private BroadcastReceiver netReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if(action == null)
				return;

			//Log.d(TAG, "netReceiver         action = " + action);

			if (action.equals(Intent.ACTION_TIME_TICK)){
				displayDate();		
			} else {
				displayStatus();
				updateStatus();
			}
		}
	};

 

}
