package com.amlogic.launcher;

import android.app.Activity;
import android.app.SearchManager;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.SystemProperties;
import android.os.Handler;
import android.os.Message;


import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import android.view.View;
import android.view.KeyEvent;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.View.OnFocusChangeListener;

import android.widget.BaseAdapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.GridView;
import android.widget.Button;
import android.widget.RelativeLayout;

import android.text.format.DateFormat;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.FileOutputStream;
import java.lang.Runnable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Comparator;
import java.lang.Exception;


public class LocalActivity extends Activity {
    /** Called when the activity is first created. */
	private static final String TAG = "LocalActvity";

	public static final String LOCAL_DISK_ROOT = "/storage";
	public static final String SHEILD_EXT_STOR = "/storage/sdcard0/external_storage";
	public static final String NAND_PATH = "/storage/sdcard0";
	public static final String SD_PATH = "/storage/external_storage/sdcard1";
	public static final String USB_PATH ="/storage/external_storage";
	public static final String SATA_PATH ="/storage/external_storage/sata";
	private final String DATA_PATH = "/data/data/com.amlogic.launcher/";
	private final String HIDE_STATUSBAR = "sys.hideStatusBar.enable";
	
	private GridView gv = null;
	private boolean into_mate_activity = false;
	
	private ImageButton btn_home = null;
	private TextView tx_home = null;
	private  AnimationDrawable anim_selector_1 = null;
	private  AnimationDrawable anim_selector_2 = null;
	private GridView lv_menu = null;
	private GridView lv_status = null;

	private RelativeLayout layout_main = null; 
	private Dialog dialog_wallpaper = null;
	
	private Drawable mDrawable = null;
	private Bitmap mBmp = null;
	private boolean into_home = false;
	private String curPath;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	 	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);  
        setContentView(R.layout.main_local);
		Log.d(TAG, "------onCreate");
		
		layout_main = (RelativeLayout)findViewById(R.id.layout_main);
		if (Launcher.img_bg != null){
			mDrawable = Launcher.img_bg;
			mBmp = Launcher.bmp_bg;
			layout_main.setBackgroundDrawable(mDrawable);		
		} 

		SystemProperties.set(HIDE_STATUSBAR,"true");

		tx_home = (TextView)findViewById(R.id.tx_local);
		tx_home.setBackgroundResource(R.drawable.bg_menu_tx);

		curPath = LOCAL_DISK_ROOT;
		gv = (GridView)findViewById(R.id.grid_local);
		anim_selector_1 = (AnimationDrawable)gv.getSelector();

		
		lv_status = (GridView)findViewById(R.id.list_status);
		

		lv_menu = (GridView)findViewById(R.id.list_menu);
		anim_selector_2 = (AnimationDrawable)lv_menu.getSelector();
		lv_menu.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(pos);
				Intent intent = new Intent();
				
				Launcher.playClickMusic();
			
			//	into_mate_activity = true;	
		            if(item.get("item_type").equals(R.drawable.btn_home)) {
						into_home = true;
					  	intent.setClass(LocalActivity.this, Launcher.class);
					  	startActivity(intent);
		            }else if(item.get("item_type").equals(R.drawable.btn_apps)){
						intent.setClass(LocalActivity.this, AllApps.class);
						startActivity(intent);
					}else if(item.get("item_type").equals(R.drawable.btn_browser)){
						intent.setClass(LocalActivity.this, BrowserActivity.class);
						startActivity(intent);
					}else if(item.get("item_type").equals(R.drawable.btn_setting)){
						intent .setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
						startActivity(intent);
					}   
			     
            }
        });
	
        gv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(pos);

				Launcher.playClickMusic();
				
                if(item.get("item_type").equals(R.drawable.item_type_back)) {
					File file = new File(curPath);
					String parent_path = file.getParent();
					if(curPath.equals(NAND_PATH)||curPath.equals(SD_PATH)||parent_path.equals(USB_PATH)) {
						curPath = LOCAL_DISK_ROOT;
					}
			      else {
			          curPath = parent_path;
			      }
		      	  displayView();
		      	  return;
              }
              if(item.get("item_sel").equals(R.drawable.item_img_sel)) {
              } else {
		      		String file_path = (String) item.get("file_path");
		     		File file = new File(file_path);
                    if (file.isDirectory()) {	
		          	curPath = file_path;
		         	displayView();
	             } else {
					openFile(file);
				 }
                }
            }
        });

	dialog_wallpaper = buildDialog(this);
	gv.setOnItemLongClickListener(new OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id) {
             final Map<String, Object> item = (Map<String, Object>)parent.getItemAtPosition(pos);

			   try {
	              if(item.get("item_type").equals(R.drawable.item_type_photo)) {
						dialog_wallpaper.show();
						
						Button btn_set_wallpaper = (Button)dialog_wallpaper.findViewById(R.id.btn1);
						btn_set_wallpaper.setOnClickListener(new OnClickListener(){
							public void onClick(View v) {
								String file_name = (String)item.get("item_name");
								String src_path = (String)item.get("file_path");
								String des_path = DATA_PATH + file_name;

								copyfile(src_path, des_path, true);
								
								Bitmap bmp_bg = BitmapFactory.decodeFile(des_path, null); 
								
								BitmapDrawable draw_bg = new BitmapDrawable(bmp_bg);
								layout_main.setBackgroundDrawable(draw_bg);		

								if (mDrawable != null){
									mDrawable.setCallback(null);
								}	
								if (mBmp != null){
									mBmp.recycle();
								}
								Launcher.img_bg = draw_bg;
								mDrawable = draw_bg;
								mBmp = bmp_bg;
								saveWallpaper(file_name);					
								//Log.d(TAG, "@@@@@@@@@@@@@@@ set bg");
								
								dialog_wallpaper.dismiss();		
							}	
						});
										
	              }
			  } catch(Exception e) {
				 e.printStackTrace();
			  }

			  return true;
            }
        });
	
		gv.setOnKeyListener(Launcher.myOnKeylistener);
		lv_menu.setOnKeyListener(Launcher.myOnKeylistener);
		
		
		animStart();
		displayStatus();
		displayMenu();
		displayView();

    }

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "------onResume");
		into_mate_activity = false;
		SystemProperties.set(HIDE_STATUSBAR,"true");
		
        if(SystemProperties.getBoolean("ro.platform.has.mbxuimode", false)){
            if(SystemProperties.getBoolean("ubootenv.var.has.accelerometer", true)
                            && SystemProperties.getBoolean("sys.keeplauncher.landcape", false))
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            else
               setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
        
		IntentFilter intentFilter = new IntentFilter(Intent.ACTION_MEDIA_MOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_EJECT);
        intentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        intentFilter.addDataScheme("file");
        registerReceiver(mMountReceiver, intentFilter);

		
		IntentFilter filter = new IntentFilter();
		filter.addAction(Launcher.net_change_action);
		filter.addAction(Launcher.wifi_signal_action);
		filter.addAction(Intent.ACTION_TIME_TICK);	
		registerReceiver(netReceiver, filter);
		
		displayDate(); 
		animStart();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "------onPause");
		
		if(!into_mate_activity) {
				SystemProperties.set(HIDE_STATUSBAR,"false");
				into_mate_activity = false;
		}
		 
		animStop();
		unregisterReceiver(mMountReceiver);
		unregisterReceiver(netReceiver);
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "------onStop");

		if (into_home == true){	
			if (mDrawable != null){
				mDrawable.setCallback(null);
			}	
			if (mBmp != null){
				mBmp.recycle();
			}
			into_home = false;
		}
	
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "------onDestroy");
	}
	 
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	  if(keyCode == KeyEvent.KEYCODE_BACK) {
	  	 Intent intent = new Intent();
		 intent.setClass(LocalActivity.this, Launcher.class);
		 startActivity(intent);
		 
		 return true;
	  }

      if(keyCode == KeyEvent.KEYCODE_SEARCH) {        
          SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
          ComponentName globalSearchActivity = searchManager.getGlobalSearchActivity();
          if (globalSearchActivity == null) {
              Log.w(TAG, "No global search activity found.");
             return false;
         }
          
         Intent intent = new Intent(SearchManager.INTENT_ACTION_GLOBAL_SEARCH);
         intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
         intent.setComponent(globalSearchActivity);
         Bundle appSearchData = new Bundle();
         appSearchData.putString("source", "launcher-search");
         intent.putExtra(SearchManager.APP_DATA, appSearchData);
         startActivity(intent);
		 return true;
	  }
		 return super.onKeyDown(keyCode, event);
	}
	 
	private  void animStart(){
		if (Launcher.hasAnimation == true){
			anim_selector_1.start();
			anim_selector_2.start();
		} else {
			anim_selector_1.stop();
			anim_selector_2.stop();
		}
	}
	private  void animStop(){
		anim_selector_1.stop();
		anim_selector_2.stop();
		anim_selector_1.setCallback(null);
		anim_selector_2.setCallback(null);
	}

	private void displayStatus() {
		WifiManager mWifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
        WifiInfo mWifiInfo = mWifiManager.getConnectionInfo();
        int wifi_rssi = mWifiInfo.getRssi();
		int wifi_level = WifiManager.calculateSignalLevel(
                        wifi_rssi, 5);
	
	 
		 LocalAdapter ad = new LocalAdapter(LocalActivity.this,
						 Launcher.getStatusData(wifi_level, isEthernetOn()),
						 R.layout.homelist_item, 			 
									 new String[] {"item_type", "item_name", "item_sel"},
									 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
		 lv_status.setAdapter(ad);
	}

	private void updateStatus() {
		   ((BaseAdapter) lv_status.getAdapter()).notifyDataSetChanged();
	}

	private void displayDate() {
			int[] array = Launcher.getTime();
			
			ImageView hour_1 = (ImageView)findViewById(R.id.img_hour_1);	
			ImageView hour_2 = (ImageView)findViewById(R.id.img_hour_2);
			ImageView colon = (ImageView)findViewById(R.id.img_colon);
			ImageView minu_1 = (ImageView)findViewById(R.id.img_minu_1);
			ImageView minu_2 = (ImageView)findViewById(R.id.img_minu_2);	
			ImageView day = (ImageView)findViewById(R.id.img_day);
		
	
			BrowserActivity.setImage(hour_1, array[0]);
			BrowserActivity.setImage(hour_2, array[1]);
			colon.setImageResource(R.drawable.s_colon);
			BrowserActivity.setImage(minu_1, array[2]);
			BrowserActivity.setImage(minu_2, array[3]);
	
			Launcher.is24hFormart = DateFormat.is24HourFormat(this); 
			if (array[4] != -1) {
				day.setVisibility(View.VISIBLE);
				if (array[4] == 0) {
					day.setImageResource(R.drawable.s_am);
				} else{
					day.setImageResource(R.drawable.s_pm);
				}
			} else {
				day.setVisibility(View.GONE);
			}
		

			ImageView weather = (ImageView)findViewById(R.id.img_weather);
			TextView  temperature = (TextView)findViewById(R.id.tx_temperature);
			
			weather.setImageDrawable(Launcher.img_weather);
			temperature.setText(Launcher.temperature);
			temperature.setTypeface(Typeface.MONOSPACE,Typeface.BOLD_ITALIC);
			temperature.setTextSize(20);
	
	}
	

	private void displayMenu() {
	
	
			 LocalAdapter ad = new LocalAdapter(LocalActivity.this,
							 Launcher.getMenuData(),
							 R.layout.list_menu,			 
										 new String[] {"item_type", "item_name", "item_sel"},
										 new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
			 lv_menu.setAdapter(ad);
			 lv_menu.setSelection(0);
			 lv_menu.requestFocus();	 	 
	}
	
	
   private void displayView() {
   	
        LocalAdapter ad = new LocalAdapter(LocalActivity.this,
        				getListData(),
        				R.layout.devicegrid_item,        		
                                 	new String[] {"item_type", "item_name", "item_sel"},
                                 	new int[] {R.id.item_type, R.id.item_name, R.id.item_sel,});
        gv.setAdapter(ad);;
    }

   private void updateView() {
		 ((BaseAdapter) gv.getAdapter()).notifyDataSetChanged();
	}

   private boolean isEthernetOn(){
   		ConnectivityManager connectivity = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
	
		if (info.isConnected()){
			return true;
		} else {
			return false;
		}
	}
	
   private List<Map<String, Object>> getListData() {
		   if(LOCAL_DISK_ROOT.equals(curPath)) {
			return getDeviceList();
		   }

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();	
		Map<String, Object> map = new HashMap<String, Object>();	
		map.put("item_name", getText(R.string.str_back));
		map.put("file_path", "..");    
		map.put("item_sel", R.drawable.item_img_unsel);
		map.put("item_type", R.drawable.item_type_back);
		list.add(map); 
		
			try {
		  // Log.d(TAG, "getListData: path[" + curPath + "]");
			   File file_path = new File(curPath); 
			   if (file_path != null && file_path.exists()) { 
				   if (file_path.listFiles() != null) {
						   if (file_path.listFiles().length > 0) {
							   for (File file : file_path.listFiles()) {					   
									   String file_abs_path = file.getAbsolutePath();
									   if (file_abs_path.equals(SHEILD_EXT_STOR))
										   continue;
									   if ((file.isDirectory()) && (!file.isHidden())) {
										   map = new HashMap<String, Object>(); 					   
									   	   map.put("item_name", file.getName());   
										   map.put("file_path", file_abs_path); 		   
										   map.put("item_sel", R.drawable.item_img_unsel);
										   map.put("item_type", R.drawable.item_type_dir);
										   list.add(map);
									   } else if(getFileTypeImg(file_abs_path) != null){
									  	   map = new HashMap<String, Object>(); 
										   map.put("item_name", file.getName());   
										   map.put("file_path", file_abs_path); 		   
										   map.put("item_sel", R.drawable.item_img_unsel);
										   map.put("item_type", getFileTypeImg(file_abs_path));
										   list.add(map);

									   }
							   }
						   }				   
				   }
			   }
			   } catch (Exception e) {
			   Log.e(TAG, "Exception when getFileListData(): ", e);
			   return list;
		   }   
			   
				/*   if (!list.isEmpty()) {  
				   Collections.sort(list, new Comparator<Map<String, Object>>() {
					   public int compare(Map<String, Object> object1,
							   Map<String, Object> object2) {  
						   File file1 = new File((String) object1.get("file_path"));
						   File file2 = new File((String) object2.get("file_path"));
						   
						   if (file1.isFile() && file2.isFile() || file1.isDirectory() && file2.isDirectory()) {
							   return ((String) object1.get("item_name")).toLowerCase()
									   .compareTo(((String) object2.get("item_name")).toLowerCase());					   
						   } else {
							   return file1.isFile() ? 1 : -1;
						   }			   
					   }			   
				   });	
				   }*/
		   return list;
	  }   

	private List<Map<String, Object>> getDeviceList() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(); 
        Map<String, Object> map;
       // Log.d(TAG, "getDeviceList: path[" + LOCAL_DISK_ROOT + "]");
		
		File dir = new File(NAND_PATH);
		if (dir.exists() && dir.isDirectory()) {
			map = new HashMap<String, Object>();
			map.put("item_name", getText(R.string.memory_device_str));
			map.put("file_path", NAND_PATH);
			map.put("item_type", R.drawable.memory_icon);
			map.put("item_sel", R.drawable.item_img_unsel);
			list.add(map);
		}

//		dir = new File(SD_PATH);
//		if (dir.exists() && dir.isDirectory()) { 
		if(Launcher.isSdcardExists()) {
			map = new HashMap<String, Object>();
			map.put("item_name", getText(R.string.sdcard_device_str));
			map.put("file_path", SD_PATH);
			map.put("item_type", R.drawable.sd_card_icon);
			map.put("item_sel", R.drawable.item_img_unsel);
			list.add(map);
		}

		dir = new File(USB_PATH);
		if (dir.exists() && dir.isDirectory()) { 
			if (dir.listFiles() != null) {
				int dev_count=0;
				for (File file : dir.listFiles()) {
					if (file.isDirectory()) {
						String devname = null;
						String path = file.getAbsolutePath();
						if (path.startsWith(USB_PATH+"/sd")&&!path.equals(SD_PATH)) {
							map = new HashMap<String, Object>();
							dev_count++;
							char data = (char) ('A' +dev_count-1);
							devname =  getText(R.string.usb_device_str) +"(" +data + ":)" ;
							map.put("item_name", devname);
							map.put("file_path", path);
							map.put("item_type", R.drawable.usb_card_icon);
							map.put("item_sel", R.drawable.item_img_unsel);
							list.add(map);	
						}
					}
				}
			}
		}

		dir = new File(SATA_PATH);
		if (dir.exists() && dir.isDirectory()) { 
			map = new HashMap<String, Object>();
			map.put("item_name", getText(R.string.sata_device_str));
			map.put("file_path", SATA_PATH);
			map.put("item_type", R.drawable.sata_icon);
			map.put("item_sel", R.drawable.item_img_unsel);
			list.add(map);	
		}

		return list;
    }

	protected void openFile(File f) {
			// TODO Auto-generated method stub	
			Intent intent = new Intent();
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setAction(android.content.Intent.ACTION_VIEW);
			String type = "*/*";		
			type = CheckMediaType(f);
			intent.setDataAndType(Uri.fromFile(f),type);
			startActivity(intent);				
	}


	public static Object getFileTypeImg(String filename) { 
    	if (isMusic(filename)) {
    		return R.drawable.item_type_music;
    	} else if (isPhoto(filename)) {
    		return R.drawable.item_type_photo;
    	} else if (isVideo(filename)) {
    		return R.drawable.item_type_video;
    	} else
    		return null;
    }

	    /** get file type op*/
    public static boolean isVideo(String filename) {     
    	String name = filename.toLowerCase();
        for (String ext : video_extensions) {
            if (name.endsWith(ext))
                return true;
        }
        return false;
    }
    public static boolean isMusic(String filename) {  
    	String name = filename.toLowerCase();
        for (String ext : music_extensions) {
            if (name.endsWith(ext))
                return true;
        }
        return false;
    }
    public  static boolean isPhoto(String filename) {   
    	String name = filename.toLowerCase();
        for (String ext : photo_extensions) {
            if (name.endsWith(ext))
                return true;
        }
        return false;
    } 

    
    
    /* file type extensions */
    //video from layer
    public static final String[] video_extensions = { ".3gp",
        ".divx",
        ".h264",
        ".avi",
        ".m2ts",
        ".mkv",
        ".mov",
        ".mp2",
        ".mp4",
        ".mpg",
        ".mpeg",
        ".rm",
        ".rmvb",
        ".wmv",
        ".ts",
        ".tp",
        ".dat",
        ".vob",
        ".flv",
        ".vc1",
        ".m4v",
        ".f4v",
        ".asf",
        ".lst",
        ".m2v",
        ".mts",
       /* "" */
    };
    //music
    private static final String[] music_extensions = { ".mp3",
    	".wma",
    	".m4a",
    	".aac",
    	".ape",
    	".ogg",
    	".flac",
    	".alac",
    	".wav",
    	".mid",
    	".xmf",
    	".mka",
    	".pcm",
    	".adpcm"
    };
    //photo
    private static final String[] photo_extensions = { ".jpg",
    	".jpeg",
    	".bmp",
    	".tif",
    	".tiff",
    	".png",
    	".gif",
    	".giff",
    	".jfi",
    	".jpe",
    	".jif",
    	".jfif",
    	".mpo",
    	".3dg",
    	"3dp"
    };	
    public static String CheckMediaType(File file){
        String typeStr="application/*";
        String filename = file.getName();
        
        if (isVideo(filename))
        	typeStr = "video/*";
        else if (isMusic(filename))
        	typeStr = "audio/*";
        else if (isPhoto(filename))
        	typeStr = "image/*";
        else {
			typeStr = null;
        }
        
        return typeStr;
       
    }
 

	private Handler mHandler = new Handler() {
			public void handleMessage(Message msg) {
				switch(msg.what) {
					case Launcher.UPDATE_INFO:
						displayDate();
						break;
					default:
						break;
				}
			}
	};

	private BroadcastReceiver mMountReceiver = new BroadcastReceiver() {
		
        @Override
        public void onReceive(Context context, Intent intent) {
            // Log.d(TAG, "receive broadcast:  "+intent.getAction());

			displayStatus();
			updateStatus();
			 
	         if(LOCAL_DISK_ROOT.equals(curPath)) {
		         displayView();
			     return;
       		 }
			
		     String action = intent.getAction();
	         Uri uri = intent.getData();
	         String path = uri.getPath();    
				
		     if (action == null || path == null)
	            	return;
	            
		     if (curPath.startsWith(path)) {
		         curPath = LOCAL_DISK_ROOT;
			  	 displayView();
		     }
        }
    };
	
	private BroadcastReceiver netReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if(action == null)
				return;

			//Log.d(TAG, "netReceiver         action = " + action);

			if (action.equals(Intent.ACTION_TIME_TICK)){
				displayDate();		
			} else {
				displayStatus();
				updateStatus();
			}
		}
	};

   public boolean saveWallpaper(String path) {
		File mFile = new File(Launcher.WALLPAPER_PATH);
		if(!mFile.exists()) {
			try {
				mFile.createNewFile();
			}
			catch (Exception e) {
				Log.e(TAG, e.getMessage().toString());
				return false;
			}
		}
		//Debug.d(TAG, "save string is " + s_string);
		try {
			FileWriter fw = new FileWriter(mFile);
			fw.write(path);
			fw.close();
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}
   
	private void copyfile(String srcPath, String desPath,Boolean rewrite )
	{
		File fromFile=new File(srcPath);
		File toFile=new File(desPath);	
	
		if (!fromFile.exists()) {
			return;
		}
		if (!fromFile.isFile()) {
			return ;
		}
		if (!fromFile.canRead()) {
			return ;
		}

		if (!toFile.getParentFile().exists()) {
			toFile.getParentFile().mkdirs();
		}
		if (toFile.exists() && rewrite) {
			toFile.delete();
		}

		try {
			java.io.FileInputStream fosfrom = new java.io.FileInputStream(fromFile);
			java.io.FileOutputStream fosto = new FileOutputStream(toFile);
			byte bt[] = new byte[1024];
			int c;
			while ((c = fosfrom.read(bt)) > 0) {
				fosto.write(bt, 0, c); 
			}
			fosfrom.close();
			fosto.close();
		} catch (Exception ex) {
			Log.e("readfile", ex.getMessage());
		}
	}
	
   	private Dialog buildDialog(Context context) {
   	
    	final Context con = context;
        Dialog mdialog = new Dialog(this,R.style.theme_dialog);  
        mdialog.setContentView(R.layout.layout_dialog_wallpaper); 

		
		LayoutParams params = mdialog.getWindow().getAttributes();				  
		params.y =	-100;				
		mdialog.getWindow().setAttributes(params);
		
		Button btn_recover_wallpaper = (Button)mdialog.findViewById(R.id.btn2);
		btn_recover_wallpaper.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				File mFile = new File(Launcher.WALLPAPER_PATH);
				if(mFile.exists()) {
					try {
						FileReader fr = new FileReader(mFile);
						char[] buf = new char[200];
						int len = fr.read(buf);
						String path =  DATA_PATH + new String(buf, 0, len);
						File wallpaper = new File(path);
						if (wallpaper.exists()){	
							wallpaper.delete();
						}
						fr.close();
						
						mFile.delete();
						if (Launcher.loadFactoryWallpaper() != -1){
							layout_main.setBackgroundDrawable(Launcher.img_bg);
						}else {
							layout_main.setBackgroundResource(R.drawable.bg);
							Launcher.img_bg = null;
						}			
					}
					catch (Exception e) {
						Log.e(TAG, e.getMessage().toString());
					}
				}	
				dialog_wallpaper.dismiss();		
			}	
		});

		
		Button btn_cancel  = (Button)mdialog.findViewById(R.id.btn3);
		btn_cancel.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				dialog_wallpaper.dismiss();		
			}	
		});
		
		
        return mdialog;
    }
		
	
}
